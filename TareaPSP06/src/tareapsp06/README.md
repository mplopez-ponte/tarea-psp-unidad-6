### Tarea para PSP06.

## Actividad 6.1. Crea una aplicación que realice los siguientes pasos:


+ Solicita el nombre del usuario que va a utilizar la aplicación. El login tiene una longitud de 8 caracteres y está compuesto únicamente por letras minúsculas.
+ Solicita al usuario el nombre de un fichero que quiere mostrar. El nombre del fichero es como máximo de 8 caracteres y tiene una extensión de 3 caracteres.
+ Visualiza en pantalla el contenido del fichero.
    

Es importante tener en cuenta que se tiene que realizar una validación de los datos de entrada y llevar un registro de la actividad del programa.

## Actividad 6.2. Utilizando la aplicación desarrollada en la actividad anterior, configura las políticas de acceso para:


+ Firmar digitalmente la aplicación.
+ Que sólo pueda leer los datos del directorio c:/datos.

Generamos las variables para los patrones:

![Las variables](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_Las%20variables.PNG)

En la siguiente imagen vamos a definir los patrones:

![Se definen los patrones](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_Definiendo%20patrones.PNG)

En la siguiente imagen, vamos a comprobar el nombre del usuario y el nombre del fichero para validar la entrada:

![Validando la entrada](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_Comprobando%20el%20usuario.PNG)

Ahora vamos al proceso con el manejo de las excepciones:

![El manejo de las excepciones](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_Mostrando%20las%20excepciones.PNG)

El siguiente paso es generando el archivo jar (Java Archive), tal como se muestra en la imagen:

![Generando el archivo jar](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_Generando%20el%20archivo%20jar.PNG)

En este momento vamos a generar las claves de la siguiente forma, tal como se muestra en la imagen:

![Generamos las claves](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_Generando%20las%20claves.jpg)

Exportamos la clave pública:

![Exportando la clave pública](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_Exportando%20la%20llave%20p%C3%BAblica.jpg)

Ahora firmamos el archivo jar y lo renombramos a sTareaPSP06.jar:

![Firmamos el archivo jar](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_Firmando%20el%20archivo%20jar.PNG)

Ya tenemos nuestro archivo jar firmado, ahora hay que especifica la política de acceso al directorio c:/datos con permisos de lectura.

![Generando la política de acceso](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_Generando%20la%20politica%20de%20acceso.PNG)

Ahora vamos a mostrar la imagen con la política de acceso ya creada:

![La política de acceso ya creada](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/PSP06_La%20politica%20de%20acceso%20ya%20creado.PNG)

A continuación y ya por último, vamos a mostrar los distintos archivos que conforman nuestro proyecto:

![El contenido de nuestro proyecto](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-6/downloads/TareaPSP06_Los%20archivos%20del%20proyecto.JPG)

Para poder ejecutar la aplicación, la vamos a hacer a través del siguiente comando:

**java -jar "TareaPSP06.jar"**

En el último paso y para poder acceder a nuestro repositorio lo podremos hacer a través del siguiente comando de git:

La entrega será en texto plano. Lo único que tenéis que indicar son estas dos líneas:

**git clone https://mplopez-ponte@bitbucket.org/mplopez-ponte/tarea-psp-unidad-6.git**

**git checkout master**